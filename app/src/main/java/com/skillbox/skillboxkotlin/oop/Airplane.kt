package com.skillbox.skillboxkotlin.oop

class Airplane(maxSpeed: Int) : Vehicle (maxSpeed) {

    override fun getTitle(): String = "Airplane"

}