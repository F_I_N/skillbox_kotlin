package com.skillbox.skillboxkotlin.oop

class Rectangle(
    x: Int,
    y:Int,
    private val width: Int,
    private val height: Int
): AbstractShape(x, y) {   //создание дочернего класса к абстрактому чтобы вызываь его  //alt + enter -> implement members

    override val name: String = "Rectangle"

    override fun calculateArea(): Double = width * height.toDouble()
}