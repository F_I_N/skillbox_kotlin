package com.skillbox.skillboxkotlin.oop

class CarVehicle(
    val wheelCount: Int,
    val doorCount: Int,
    maxSpeed: Int
) : Vehicle(maxSpeed) {

    var isDoorOpen: Boolean = false
    private set

    private lateinit var driver: User

    private val engine by lazy {
        Engine()
    }



    operator fun component1 (): Int = wheelCount   //переопределение componentN
    operator fun component2 (): Int = doorCount



    fun openDoor() {
        if (!isDoorOpen) {
            println("Дверь открыта")
        }
        isDoorOpen = true
    }

    fun closeDoor() {
        if (isDoorOpen) {
            println("Дверь зыкрыта")
            if (::driver.isInitialized) {    //доступ к свойству
                println("driver = $driver")
            }

        }
        isDoorOpen = false


    }

    override fun accelerate(speed: Int) {
        engine.use()
        if (isDoorOpen) {
            println("Вы не можете ускориться с открытой дверью")
        } else {
            super.accelerate(speed)  //вызываем реализацию этого метода у родительского класса
        }
    }


    fun setDriver(driver: User) {
        this.driver = driver
    }



    override fun getTitle(): String = "Car"

    fun accelerate (speed: Int, force: Boolean) {   //перегрузка метода accelerate
        if (force) {
            if (isDoorOpen) println("Внимание, ускорение с открытой дверью!")
            super.accelerate(speed)

        } else {
            accelerate(speed)
        }

    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CarVehicle

        if (wheelCount != other.wheelCount) return false
        if (doorCount != other.doorCount) return false
        if (isDoorOpen != other.isDoorOpen) return false

        return true
    }

    override fun hashCode(): Int {
        var result = wheelCount
        result = 31 * result + doorCount
        result = 31 * result + isDoorOpen.hashCode()
        return result
    }


    companion object {
        val default = CarVehicle(4,4,200)

        fun createWithDefaultCount(doorCount: Int, maxSpeed: Int): CarVehicle {
            return CarVehicle(wheelCount = 4, doorCount= doorCount, maxSpeed = maxSpeed)

        }
    }


}