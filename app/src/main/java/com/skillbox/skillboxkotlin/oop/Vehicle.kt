package com.skillbox.skillboxkotlin.oop

open class Vehicle(val maxSpeed: Int) {




    var currentSpeed: Int = 0
        get() {
            println("currentSpeed get")
            return field
        }
        private set(value) {
            println("currentSpeed set $value")
            field = value
        }

    var fuelCount: Int = 0
        protected set           //мы можем установить наше количество топлива из класса наследника, но из других мест установка топлива будет не доступна


    open fun getTitle(): String {
        return "Vehicle"
    }


    open fun accelerate(speed: Int){

        val needFuel = speed / 2

        if (fuelCount >= needFuel) {
            currentSpeed += speed
            useFuel(needFuel)
        } else println("Car need more fuel for accelerate")
    }

    fun decelerate(speed: Int){

        currentSpeed = maxOf(0, currentSpeed - speed)
    }

    open protected fun useFuel(fuelCount: Int){  // можно использовать в наследниках и переопределить его
        this.fuelCount -= fuelCount

    }

    fun refuel(fuelCount: Int) {
        if (currentSpeed == 0) {
            this.fuelCount += fuelCount   //обращение к свойству текущего объекта
        } else println("You cant refuel because car speed is not 0")
    }



}