package com.skillbox.skillboxkotlin.oop

class Car constructor(
    val wheelCount: Int,
    val doorsCount: Int,
    val bodyLength: Int,
    val bodyWidth: Int,
    val bodyHeight: Int


) {

//    init {
//        println("Car init")
//    }

    var currentSpeed: Int = 0
        get() {
            println("currentSpeed get")
            return field
        }
        private set(value) {
            println("currentSpeed set $value")
            field = value
        }

    var fuelCount: Int = 0
        private set           //можем обращаться для чтения, но не  можем установить значение

//    val isStoped: Boolean
//        get() = currentSpeed == 0

//    constructor(wheelCount: Int, doorsCount: Int, bodySize: Triple<Int, Int, Int>): this(wheelCount, doorsCount, bodySize.first, bodySize.second, bodySize.third) {
//        println("Car secondary constructor")
//    }

    fun accelerate(speed: Int){

        val needFuel = speed / 2

        if (fuelCount >= needFuel) {
            currentSpeed += speed
            useFuel(needFuel)
        } else println("Car need more fuel for accelerate")
    }

    fun decelerate(speed: Int){

        currentSpeed = maxOf(0, currentSpeed - speed)
    }

    private fun useFuel(fuelCount: Int){
        this.fuelCount -= fuelCount

    }

    fun refuel(fuelCount: Int) {
        if (currentSpeed == 0) {
            this.fuelCount += fuelCount   //обращение к свойству текущего объекта
        } else println("You cant refuel because car speed is not 0")
    }



}