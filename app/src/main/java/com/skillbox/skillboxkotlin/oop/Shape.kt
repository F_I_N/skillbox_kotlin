package com.skillbox.skillboxkotlin.oop

interface Shape {

    val name : String

    fun calculateArea(): Double


}