package com.skillbox.skillboxkotlin.oop

import com.skillbox.skillboxkotlin.basic.PrintAreaOnChangeDelegate

class Circle(
    radius: Int
): Shape {

    var radius: Int by PrintAreaOnChangeDelegate(
        radius
    )


    override val name: String = "Circle"

    override fun calculateArea(): Double {
        return Math.PI * radius * radius
    }

}