package com.skillbox.skillboxkotlin.oop

import com.skillbox.skillboxkotlin.basic.PrintAreaOnChangeDelegate

class RectangleInterface (
    x: Int,
    y:Int,
    width: Int,
    height: Int
): AbstractShapeInterface(x, y), Comparable<RectangleInterface> {   //создание дочернего класса к абстрактому чтобы вызываь его  //alt + enter -> implement members

    var width: Int by PrintAreaOnChangeDelegate(
        width
    )  //при обращении и изменении будут вызываться методы getValue s setValue из делегированного свойства
    var height: Int by PrintAreaOnChangeDelegate(
        height
    )





    override val name: String = "Rectangle"

    override fun calculateArea(): Double = width * height.toDouble()

    override fun compareTo(other: RectangleInterface): Int {
        return (width + height) - (other.width + other.height)
    }

    override fun toString(): String {   // alt + insert - автоматическая генерация toString
        return "RectangleInterface(width=$width, height=$height)"
    }


//    override fun toString(): String {
//        return "Rectangle(width = $width, height = $height)"
//    }



    // ПЕРЕГРУЗКА ФУНКЦИИ
    operator fun plus(other: RectangleInterface) : RectangleInterface {
        return RectangleInterface(0, 0, width + other.width, height + other.height)  // other это объект который стоит после плюса в Operators.kt
    }

    operator fun unaryMinus(): RectangleInterface {
        return RectangleInterface(0, 0, -width, -height)

    }










}