package com.skillbox.skillboxkotlin.oop

interface Drawable {
    fun draw()
}