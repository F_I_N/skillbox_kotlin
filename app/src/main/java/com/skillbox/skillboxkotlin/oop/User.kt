package com.skillbox.skillboxkotlin.oop

data class User(
    val name: String,
    val lastName: String
) {
    var innerState: String = ""
}