package com.skillbox.skillboxkotlin.basic

fun main() {
    val pair1 = Pair("key", "value")
    val pair2 = 1 to 2

    println("first = ${pair1.first}, second = ${pair1.second}")
    println(pair2)

    val map = mapOf(
        "Name1" to "+79991230001",
        "Name2" to "+79991230002",
        "Name3" to "+79991230003"
    )

    println(map["Name1"])
    println(map["Name4"])


    val map1 = mapOf(                  //когда создаем mapOf то создается linked hashMap
        "Name1" to "+79991230001",
        "Name1" to "+79990000001",
        "Name2" to "+79991230002",
        "Name3" to "+79991230003"
    )
    println(map["Name1"])


    //далее мутабельная мапа
    val mutableMap = map.toMutableMap()
    val mutableMap2 = mutableMapOf("1" to "2")
    mutableMap["Name1"] = "1231564"  //добавление значения
    mutableMap["Name2"] = "9875422"
    mutableMap.put("key", "value") //добавление ключа  и значения
    mutableMap.remove("key") //удаление


    //приведение мутабельной мапы к немутабельной
    mutableMap.toMap()

    val sortedMap = sortedMapOf(
        "2" to "22",
        "3" to "33",
        "4" to "44",
        "1" to "11"
    )

    val hashMap = hashMapOf(
        "2" to "22",
        "3" to "33",
        "4" to "44",
        "1" to "11"
    )

    println(sortedMap)
    println(hashMap)


    //у мапы можно получить доступ к ключам и множеству значений
    println(hashMap.keys)
    println(hashMap.values)

    //итерация по мапе
    println("Итерация по мапе")
    for (key in hashMap.keys) {
        println("key = $key, value = ${hashMap[key]}")
    }

    hashMap.forEach { entry -> entry.key
        entry.value
    }

    hashMap.forEach { (key, value) ->  //то же самое
    key
    value
    }






}