package com.skillbox.skillboxkotlin.basic

fun main() {
    var helloWordString: String = "Hello World"
    println(helloWordString)

    val array = arrayListOf(1)
    array.add(2)
    array.add(3)

    println(array)

    val shortVal: Short = 123
    val sdfg: Double = 12.22
    val multiString = """
        dfdfgdfg
        fartuy
        """
    println(multiString)

    val name = "Ivan"
    val lastname = "Ivanov"
    val isMale = true

    val readableName = "My name is ${name} and last name is ${lastname}"
    println(readableName)

    val readableExpanded = "${if (isMale) "His" else "Her"} name is ${name} and last name is ${lastname}"
    println(readableExpanded)






}