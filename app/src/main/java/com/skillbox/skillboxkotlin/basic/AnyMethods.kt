package com.skillbox.skillboxkotlin.basic


import com.skillbox.skillboxkotlin.oop.CarVehicle

fun main() {
    val car1 = CarVehicle (wheelCount = 4, doorCount = 4, maxSpeed = 200)
    val car2 = CarVehicle (wheelCount = 4, doorCount = 4, maxSpeed = 200)
    val car3 = car1


    println("car1 equals to car2 by reference =  ${car1 === car2}")  //проверка равенста по ссылке
    println("car1 equals to car2 by reference =  ${car1 === car3}")
    println("car1 equals to car2 by reference =  ${car1 == car2}") //проверка равенста по значению




    val cars = listOf(
        car1,
        CarVehicle(wheelCount = 2, doorCount = 2, maxSpeed = 100),
        CarVehicle(wheelCount = 2, doorCount = 3, maxSpeed = 100)
    )

    println(cars.contains(CarVehicle(wheelCount = 2, maxSpeed = 100, doorCount = 2)))


    val map = hashMapOf(
        car1 to "1",
        car2 to "2",
        CarVehicle(wheelCount = 2, doorCount = 2, maxSpeed = 100) to "3"
    )

    println(map[CarVehicle (wheelCount = 4, doorCount = 4, maxSpeed = 200)])
}