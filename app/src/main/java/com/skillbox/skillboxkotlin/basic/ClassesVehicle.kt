package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.CarVehicle
import com.skillbox.skillboxkotlin.oop.Vehicle

fun main() {

    val tesla = CarVehicle(4, 2, 150)

    with(tesla) {
        refuel(100)
        openDoor()
        accelerate(100)
        accelerate(100, true)  //перезагрузка метода
        decelerate(50)

    }

//    val vehicle = Vehicle(maxSpeed = 100)
//    vehicle.accelerate(100)

    val vehicle: Vehicle = tesla  // объекты класса наследника могут выступать в качестве объектов родительского класса

    println("vehicle is car = ${ vehicle is CarVehicle}") // является ли vehicle объектом класса car
    println("tesla is vehicle = ${ tesla is Vehicle}")
    println("vehicle is car = ${ Vehicle(maxSpeed = 100) is CarVehicle}")

    val newCar: CarVehicle = vehicle as CarVehicle //приведение объекта vehicle к типу car
    newCar.closeDoor()

//    val newCar1: CarVehicle? = vehicle as? CarVehicle      safe call оператор для безопасного определения, если не является то на выхоже null
//    newCar1?.closeDoor()






}