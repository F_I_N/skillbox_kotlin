package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.CarVehicle
import com.skillbox.skillboxkotlin.oop.Circle
import com.skillbox.skillboxkotlin.oop.RectangleInterface
import com.skillbox.skillboxkotlin.oop.Shape
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun main() {

    val rectangle = RectangleInterface(0,0,10,20)
    rectangle.width = 15
    rectangle.height = 50

    val circle = Circle(10)
    circle.radius = 20

    println("--------------------------------")

    val car = CarVehicle(4, 3, 144)
    car.openDoor()
    car.closeDoor()
    car.accelerate(33)


}

class PrintAreaOnChangeDelegate <T> (
    private var value: T
) : ReadWriteProperty<Shape, T> {
    override fun getValue(thisRef: Shape, property: KProperty<*>): T = value

    override fun setValue(thisRef: Shape, property: KProperty<*>, value: T) {
        println("before change property ${property.name} = ${thisRef.calculateArea()}")
        this.value = value
        println("after change property ${property.name} = ${thisRef.calculateArea()}")
    }

}