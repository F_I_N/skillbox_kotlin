package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.RectangleInterface

fun main() {
    val shape = RectangleInterface(x = 1, y = 1, width = 2, height = 2)

    val largeRectangle = RectangleInterface(1,1,20, 30)

    val set: MutableSet<RectangleInterface> = hashSetOf(
        largeRectangle,
        shape
    )

    set.size

    println(set)


}