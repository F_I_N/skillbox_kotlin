package com.skillbox.skillboxkotlin.basic

fun main() {

    print("Введите число: ")
    val n = readLine()?.toIntOrNull() ?: return
    println("Вы ввели число: $n")



    println("Сумма с помощью while = ${calculateSumByWhile(
        n
    )}")

    println("Сумма с помощью while и return = ${calculateSumByWhileInfiniteReturn(
        n
    )}")

    println("Сумма с помощью while и break = ${calculateSumByWhileInfiniteBreak(
        n
    )}")

    println("Сумма с помощью  do while = ${calculateSumDoWhile(
        n
    )}")

    println("Сумма с помощью  for = ${calculateSumFor(
        n
    )}")

    printChars()

    println()

    printEvenNumbers(n)

    println()

    printEvenNumberFor(n)

}

fun calculateSumByWhile(n: Int): Long {
    var sum: Long = 0
    var currentNumber: Int = 0
    while (currentNumber <= n) {
        println("Iteration while $currentNumber")
        sum += currentNumber
        currentNumber++
    }

    return sum
}



fun calculateSumByWhileInfiniteReturn(n: Int): Long {     //прерывание бесконечного цикла
    var sum: Long = 0
    var currentNumber: Int = 0

    while (true) {

        if (currentNumber > n) return sum

        sum += currentNumber
        currentNumber++
    }
}

fun calculateSumByWhileInfiniteBreak(n: Int): Long {     //прерывание бесконечного цикла
    var sum: Long = 0
    var currentNumber: Int = 0

    while (true) {

        if (currentNumber > n) break

        sum += currentNumber
        currentNumber++
    }
    return sum
}


fun printEvenNumbers(n: Int) {   //вывод четных чисел
    var currentNumber = 0
    while (currentNumber <= n) {
        val numberBefore = currentNumber
        currentNumber++
        if (numberBefore % 2 == 1) continue
        println(numberBefore)

    }
}



// Сумма с помощью do while
fun calculateSumDoWhile(n: Int): Long {
    var sum: Long = 0
    var currentNumber: Int = 0

    do {
        println("Iteration do while $currentNumber")
        sum += currentNumber
        currentNumber++
    } while (currentNumber <= n)

    return sum
}


//Сумма с помощью for

fun calculateSumFor(n: Int): Long {
    var sum : Long = 0

    val range = 0..n

    for (currentNumber in range) {
        sum += currentNumber

    }
    return sum

}

// вывод всех букв string
fun printChars() {
    for (currentCh in "string") {
        println(currentCh)
    }
}



//вывод каждого четного числа с помощью for с шагом 2 в возрастающую строну
fun printEvenNumberFor(n: Int) {
    val range = 0..n step 2
    //val range = n downTo 0 step 2         //убывающее, в обратном прядке вывод
    for(currentNumber in range) {
        println(currentNumber)
    }
}










