package com.skillbox.skillboxkotlin.basic

import kotlin.random.Random

fun main() {
    println(multiply(123, 4))
    println(multiply2(100, 5))
    println(10/3)
    println(10/3f)

    hasChildAccess(
        110,
        50,
        20
    )  // ALT+ENTER - Add names to call arguments

    val Any = functionReturnsAny()


    functionWithAnyParam("string")
    functionWithAnyParam('v')
    functionWithAnyParam(3)


}

fun multiply(a: Int, b: Int): Int {
    return a * b //заинлайненная
    //val result = a * b
    //return result
}

fun multiply2(c: Int, d: Int): Int = c / d
//fun multiply2(c: Int, d: Int) = c / d

fun hasChildAccess(height: Int, weight: Int, age: Int) : Boolean {
    return height > 150 && weight >30 && age >10
}


fun functionReturnsAny(): Any {
    return if(Random.nextBoolean()) {   //если результат true, то возвращает число 23232, усли false то строку "fsdfsfsf"
        23232
    } else {
        "fsdfsfsf"
    }
}


fun functionWithAnyParam(param: Any) {

}

fun functionWithAnyParam1(param1: Any): Unit {   //Unit
    return

}

