package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.Car

fun main() {
    val tesla = Car(
        wheelCount = 4, doorsCount = 2, bodyLength = 500, bodyWidth = 200, bodyHeight = 150
    )
//
    println("tesla, number of wheels = ${tesla.wheelCount}, number of doors = ${tesla.doorsCount}, current speed is ${tesla.currentSpeed} ")
//
//    tesla.currentSpeed = 123
//    println("And now current speed is ${tesla.currentSpeed} ")

//    val nissan = Car(4,2,Triple(first = 450, second = 190, third = 140))

    tesla.accelerate(100)
    println("tesla current speed is ${tesla.currentSpeed} ")
    tesla.decelerate(50)
    println("tesla current speed is ${tesla.currentSpeed} ")
    println("tesla fuel count is ${tesla.fuelCount} ")






}