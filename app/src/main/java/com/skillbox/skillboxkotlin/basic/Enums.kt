package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.Color
import com.skillbox.skillboxkotlin.oop.SealedColor

fun main() {
    val color = Color.BLACK.hex
    Color.RED.draw()

    println("---------------")

    Color.values().forEach { it   //возвращает список всех объектов перечисления
        it.draw()
        println(it.name)
        println(it.ordinal)
        println("**************")

    }

    println("---------------")

    Color.values()[Color.RED.ordinal]

    Color.fromName("2323")


    println("↓↓↓sealed class↓↓↓")

    SealedColor.Black
    SealedColor.CustomColor("#ff0")




    println("↑↑↑sealed class↑↑↑")







    Color.valueOf("BLACK")  //возвращает объект перечисления, у которого имя совпадает с строкой, которая передается в valueOf
}

fun invertColor(color: Color): Color {
    return when(color) {
        Color.WHITE -> Color.BLACK
        Color.BLACK -> Color.WHITE
        Color.RED -> Color.BLUE
        Color.BLUE -> Color.RED
    }
}


fun invertSealedColor(color: SealedColor): SealedColor {
    return when(color) {
        SealedColor.Black -> SealedColor.White
        SealedColor.White -> SealedColor.Black
        is SealedColor.CustomColor -> {
            SealedColor.White
        }

    }
}

