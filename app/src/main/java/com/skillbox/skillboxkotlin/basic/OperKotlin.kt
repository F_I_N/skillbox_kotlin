package com.skillbox.skillboxkotlin.basic

fun main() {
    println(getDeveloperPosition(-1))
    println(getDeveloperPosition(0))
    println(getDeveloperPosition(1))
    println(getDeveloperPosition(2))
    println(getDeveloperPosition(3))
    println(getDeveloperPosition(10))


}

fun maxInt(a: Int, b: Int): Int {
    val maxValue: Int
    if (a < b) {
        maxValue = b
    }
    else {
        maxValue = a
    }
    return maxValue
}

// if (a < b) maxValue = b else maxValue = a
// return maxValue

// maxValue = if (a < b) b else a
// return maxValue

// val maxValue: Int = if (a < b) b else a
// return maxValue



// fun maxInt(a: Int, b: Int): Int {
// return if (a < b) b else a
// }

//fun maxInt(a: Int, b: Int): Int = if (a < b) b else a


fun calculatePrice(booleanParam: Boolean): Int {
    return if(booleanParam) {
        val intermediateResult = 234 + 452
        intermediateResult + 3
    } else {
        val intermediateResult = 14 + 2
        intermediateResult + 3

    }
}


fun getCarTypeWhen(maxSpeed: Int): String {
    return when {
        maxSpeed < 20 -> "Трактор"
        maxSpeed < 60 -> "Медленная машина"
        maxSpeed < 200 -> "Обычная машина"
        else -> "Быстрая машина"
    }
}

fun getCarTypeIf(maxSpeed: Int): String {
    return if (maxSpeed < 20) "Трактор"
    else if (maxSpeed < 60) "Медленная машина"
    else if (maxSpeed < 200) "Обычная машина"
    else "Быстрая машина"
}

fun getCarTypeWhenSport(maxSpeed: Int, hasSportMode: Boolean): String {
    return when {
        maxSpeed < 20 -> "Трактор"
        maxSpeed < 60 -> "Медленная машина"
        hasSportMode && maxSpeed < 200 -> "Обычная машина"
        else -> "Быстрая машина"
    }
}


fun getDeveloperPosition(experience: Int): String {
    return if(experience < 0) {
        "Incorrect experience"
    } else {
        when(experience) {
            0 -> "Intern"
            in 1..2 -> "Junior"
            in 3..4 -> "Middle"
            else -> "Senior"
        }
    }
}