package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.RectangleInterface

fun main() {
    val shape = RectangleInterface(x = 1, y = 1, width = 2, height = 2)
    shape.printPosition()
    shape.moveToPosition(2,2)
    shape.printPosition()

    println("shape area = ${shape.calculateArea()}")
    println("shape name = ${shape.name}")

    val largeRectangle = RectangleInterface(1,1,20, 30)

    println("shape <= largeRectangle (${shape <= largeRectangle})")

    val set: MutableSet<RectangleInterface> = sortedSetOf(
        largeRectangle,
        shape
    )

    println(set)


}