package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.CarVehicle
import com.skillbox.skillboxkotlin.oop.Vehicle

fun main() {

    Cars.nissan
    Cars.someMethod()
    Cars.accelerate(20)

    CarVehicle.default
    CarVehicle.createWithDefaultCount(4, 200)

    val a = CarVehicle
    val b = Cars

}

object Cars: Vehicle(100) {
    val toyota = CarVehicle(wheelCount = 4, maxSpeed = 200, doorCount = 4)
    val nissan = CarVehicle(wheelCount = 4, maxSpeed = 250, doorCount = 2)

    fun someMethod() {

    }
}