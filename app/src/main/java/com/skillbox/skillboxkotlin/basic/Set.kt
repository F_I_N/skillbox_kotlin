package com.skillbox.skillboxkotlin.basic

fun main() {
    val immutableSet = setOf(1, 2, 3, 3, 2, 1, 4, 5, 6, 7) //хранятся элементы в порядке их добавления HashSet
    println(immutableSet)

    val unionSet = setOf(1, 2, 3).union(setOf(1, 4, 5)) // объединение сетов
    println(unionSet)

    val substractSet = setOf(1, 2, 3).subtract(setOf(1, 2, 4)) //вычитание
    println(substractSet)


     val intersectSet = setOf(1, 2, 4).intersect(setOf(4, 5, 5)) // пересечение
    println(intersectSet)

    //сортировка множества по возрастанию
    val naturalOrderSet = sortedSetOf(1, 4, -1, 2, 5, 2)
    println(naturalOrderSet)

    //хранение в обратном порядке
    val naturalOrderSetDescen = sortedSetOf(1, 4, -1, 2, 5, 2)
    println(naturalOrderSetDescen.descendingSet())


    //далее изменяемый сет
    val mutableSet = sortedSetOf(1, 4, -1, 2, 5, 2).toMutableSet()

    mutableSetOf(1, 2, 3).add(34)  // добавление в сет элемента

    val hashSet = hashSetOf(1, 2, 3)
    hashSet.add(0)
    println(hashSet)

    hashSet.remove(0) //удаление элемента
    println(hashSet)

    hashSet.contains(1) //проверка, содержит ли элемент 1, на выходе  булевое значение
    1 in hashSet //то же самое
    println(1 in hashSet)


}