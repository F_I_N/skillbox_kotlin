package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.RectangleInterface

fun main() {

    listOf(1, 2, 3) + listOf(3, 4, 5)

    val sumRect = RectangleInterface(0, 0, 10, 20) + RectangleInterface(0, 0, 30, 50)

    val minusRect = -RectangleInterface(0,0, 50, 100)

    println(sumRect)
    println(minusRect)
}