package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.CarVehicle
import com.skillbox.skillboxkotlin.oop.User

fun main() {
    val user1 = User("name_1", "lastname_1")
    user1.innerState = "state_1"

    val user2 = User("name_1", "lastname_1")
    user2.innerState = "state_2"

    println(user1 == user2)


    val user3 = user1.copy(lastName = "copiedLastName")
    println("user-3 inner state = ${user3.innerState}")

    println(user1)




    val user5 = User(name = "name_5", lastName = "lastname_5")  // ишняя промежуточкая переменная
    println("name = ${user5.name}")
    println("lastname = ${user5.lastName}")

    val name = user5.component1()
    val lastname = user5.component2()
    println("name = $name")
    println("lastname = $lastname")



    val(nameDes, lastnameDes) = User(name = "name_5", lastName = "lastname_5")
    println("name = $name")
    println("lastname = $lastname")

    println("----------------")


    val users = listOf(
        user1,
        user2,
        user3
    )

    users.forEach { (name, lastname) -> println("$name, $lastname") }

    println("----------------")

    val (wheelCount, doorCount) = CarVehicle(wheelCount = 4, doorCount = 2, maxSpeed = 200)


    val map = mapOf(
        1 to "1",
        2 to "2"
    )

    for ((key, value) in map) {

    }






}