package com.skillbox.skillboxkotlin.basic

fun main() {
    print("Введите число: ")
    val n = readLine()?.toIntOrNull() ?: return
    println("Вы ввели число: $n")

    println(calculateSumRecursive(n))
}

fun calculateSumRecursive(n: Int): Int {
    return if(n == 0) {
        0
    } else {
        n + calculateSumRecursive(n - 1)
    }
}
