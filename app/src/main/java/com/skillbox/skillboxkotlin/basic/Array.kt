package com.skillbox.skillboxkotlin.basic




fun main() {
    val array = arrayOf("Петр","Иван", "Игорь")

    for(user in array) {   //каждый элемент сохраняется в переменной юзер
        println(user)
    }
    println()

    array.forEach { println(it) } // альтернативная запись

    array.forEachIndexed { index, user -> println("Index = $index, element = $user")}  //вывод индеса и элемента


    val firstElement = array[0]
    val lastElement = array[array.lastIndex]  //доступ к последнему элементу


//длину массива изменить нельзя, но можно изменить его элемент
    array[0] = "NewПетр"
    array.forEach { println(it) }


    val emptyArray = emptyArray<String>()  //создание пустого массива

    val largeArray = Array(100) { index -> "User # $index"} // создание массива из 100 пользователей
    largeArray.forEach { println(it) }

    //Можно складывать массивы
    val arraySum = arrayOf("Петр","Иван", "Игорь") + arrayOf("Новый пользователь")
    for(user in arraySum) {   //каждый элемент сохраняется в переменной юзер
        println(user)
    }





}