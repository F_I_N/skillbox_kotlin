package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.CarVehicle

fun main() {
    val car = CarVehicle(wheelCount = 4, doorCount = 4, maxSpeed = 150)
    car.openDoor()
    //car.setDriver(User("Igor", "Fedorov"))
    car.closeDoor()
}