package com.skillbox.skillboxkotlin.basic

fun main() {
    //неизменяемые списки
    val immutableList = listOf<String>("1", "2", "3")
    println(immutableList)

    immutableList[0]  //обоащение к элементам

    immutableList.forEach { println(it) }  //итерирование по элементам

    val emprtyList = emptyList<String>()  //создание пустого неизменяемого списка


    val mutableList = mutableListOf<String>("1", "2", "3") //изменяемый список
    mutableList[0] = "1.2"  //изменение первого элемента
    println(mutableList)

    mutableList.removeAt(0) //удаление первого элемента
    println(mutableList)

    mutableList.add("4") //добавление элемента в конец
    println(mutableList)

    mutableList.add(1, "new 1") //добавление второго элемента
    println(mutableList)

    mutableList.addAll(listOf("10", "20"))  //добавление элементов из другого списка
    println(mutableList)



}