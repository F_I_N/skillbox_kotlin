package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.Shape

fun main() {
    val shape = object : Shape {

        val additionalField =123

        fun additionalMethod() = 234

        override val name: String = "anonymous shape"

        override fun calculateArea(): Double = 0.0

    }

    shape.additionalField
    shape.additionalMethod()
}