package com.skillbox.skillboxkotlin.basic

fun main() {

    val string: String = "string"
    val nullableString: String? = null
    println(string.length)   //возрат длины строки
    println(nullableString?.length)


    val length: Int = string.length   // тип Int
    val nullableLength: Int? = nullableString?.length  // тип null Int

    nullableString?.reversed()
        ?.find { it == '4' }
        ?.isDigit()

    if (nullableString != null) {
        println("String length is ${nullableString.length}")

    } else {
        println("String is Null")
    }

    println("String length is ${nullableString?.length ?: "incorrect"}")  //алтернативная запись


}