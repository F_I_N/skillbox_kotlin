package com.skillbox.skillboxkotlin.basic

import com.skillbox.skillboxkotlin.oop.Airplane
import com.skillbox.skillboxkotlin.oop.CarVehicle
import com.skillbox.skillboxkotlin.oop.Vehicle

fun main() {

//    val vehicle: Vehicle = CarVehicle(wheelCount = 4, doorCount = 4, maxSpeed = 100)
//    println(vehicle.getTitle())  //из дочернего класса

    listOf<Vehicle>(
        Vehicle(maxSpeed = 100),
        CarVehicle(wheelCount = 4, doorCount = 4, maxSpeed = 100),
        Airplane(maxSpeed = 100)
    ).forEach{
        println(it.getTitle())
    }
}